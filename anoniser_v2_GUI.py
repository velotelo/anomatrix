from __future__ import division
import numpy as np
import Tkinter as tk
from tkFileDialog import askopenfilename
import tkMessageBox


ORIGINAL_MATRIX_FILE = "0_original_matrix.csv"
CLASS_PERMUT_MAT_FILE = "1_class_permutation_matrix.csv"
COL_PERMUT_MAT_FILE = "2_column_permutation_matrix.csv"
ROW_PERMUT_MAT_FILE = "3_row_permutation_matrix.csv"
KEY_FILE = "4_key_matrix.csv"
BIAS_FILE = "5_bias_matrix.csv"
ACT_FUNCS_FILE = "6_activation_functions_applied.txt"
ANON_MATRIX_FILE = "7_anonised_matrix.csv"

INT_RAND_MAX = 500   # max value used in integer matrix generator
INT_RAND_MIN = -500  # min value used in integer matrix generator

ORIG_MATRIX_INT = True  # TODO: take from UI

PRINT_PRECISION = 3  # number of decimal signs to show
CSV_PRECISION = "%.{}f".format(PRINT_PRECISION)

csv_filename = ""


def generate_integer_matrix(rows, cols):
    return np.random.randint(INT_RAND_MIN, INT_RAND_MAX, size=(rows, cols))


def generate_float_matrix(rows, cols, low=0):
    return np.random.uniform(low=low, size=(rows, cols))


def generate_key(cols, det_percent, matrix_max, matrix_min):
    min_limit = matrix_min * det_percent / 100
    max_limit = matrix_max * det_percent / 100
    if min_limit > 0:
        min_limit = 0

    # These limits don't make sense on float 0;1 matrices of up to 15x15 size
    # hence the following code: lower limit is offset from zero in both
    # directions whilst the upper limit remains unchanged, cause in this
    # situation it's unlikely that determinant will be >=1 even
    if (cols < 15):
        max_limit = det_percent / 100
        min_limit = -max_limit

    while True:
        key = generate_float_matrix(cols, cols)
        det = np.linalg.det(key)
        if ((min_limit > det or det > max_limit) and det != 0):
            return key


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def take_over_the_world():

    activation_functions_seq = list()

    def parse_csv():
        return np.loadtxt(csv_filename, delimiter=";", skiprows=1)

    def get_class_permutation_matrix(class_matrix):
        class_mart_shuffled = np.copy(class_matrix)
        np.random.shuffle(class_mart_shuffled)
        return np.column_stack((class_matrix, class_mart_shuffled))

    def calculate_anomatrix(in_matrix, iterations=1):
        anomatrix = np.matmul(in_matrix, key)
        if (use_bias.get()):
            anomatrix += bias

        function_index = np.random.randint(0, 3)
        if (function_index == 0):
            anomatrix = np.tanh(anomatrix)
            activation_functions_seq.append("Tanh, ")
        elif (function_index == 1):
            anomatrix = sigmoid(anomatrix)
            activation_functions_seq.append("Sigm, ")
        else:
            activation_functions_seq.append("Equiv, ")
        iterations -= 1
        if (iterations != 0):
            calculate_anomatrix(anomatrix, iterations)
        return anomatrix

    def write_not_used_csv(filename):
        with open(filename, "w") as file:
            file.write("Not used")

    rows = int(row_num_selector.get())
    cols = int(col_num_selector.get())

    # 1. Generate input matrix or use the CSV
    if (radio_button_value.get() == "csv" and csv_filename):
        original_matrix = np.loadtxt(csv_filename, delimiter=";", skiprows=1)
    elif (radio_button_value.get() == "csv" and not csv_filename):
        tkMessageBox.showerror("No file selected", "Please select a CSV file")
        return
    else:
        original_matrix = generate_integer_matrix(rows, cols)

    # 2. Shuffle classes and create a new matrix without class data
    if (shuffle_classes.get()):
        class_perm_matrix = get_class_permutation_matrix(original_matrix[:, -1])

    orig_matr_no_classes = np.delete(original_matrix, -1, 1)
    orig_matr_no_classes_rows, orig_matr_no_classes_cols = orig_matr_no_classes.shape

    # 3. Shuffle columns
    if (shuffle_columns.get()):
        cols_permutation = np.random.choice(
            orig_matr_no_classes_cols, orig_matr_no_classes_cols, replace=False)
        orig_matr_no_classes = orig_matr_no_classes[:, cols_permutation]

    # 4. Shuffle rows
    if (shuffle_rows.get()):
        rows_permutation = np.random.choice(
            orig_matr_no_classes_rows, orig_matr_no_classes_rows, replace=False)
        orig_matr_no_classes = orig_matr_no_classes[rows_permutation, :]

    # 5. Generate the key and bias
    key = generate_key(orig_matr_no_classes_cols, int(determinant_selector.get()),
                       original_matrix.max(), original_matrix.min())
    bias = generate_integer_matrix(rows, 1)

    # 6. Multiply by key and add bias (same value added per row) and
    #    apply the activation function
    anomatrix = calculate_anomatrix(orig_matr_no_classes,
                                    int(iteration_num_selector.get()))

    # 7 . Export results to CSV
    np.savetxt(ORIGINAL_MATRIX_FILE, original_matrix, fmt=CSV_PRECISION)
    if (shuffle_classes.get()):
        np.savetxt(CLASS_PERMUT_MAT_FILE, class_perm_matrix, fmt=CSV_PRECISION)
    else:
        write_not_used_csv(CLASS_PERMUT_MAT_FILE)
    if (shuffle_columns.get()):
        np.savetxt(COL_PERMUT_MAT_FILE, cols_permutation, fmt="%d")
    else:
        write_not_used_csv(COL_PERMUT_MAT_FILE)
    if (shuffle_rows.get()):
        np.savetxt(ROW_PERMUT_MAT_FILE, rows_permutation, fmt="%d")
    else:
        write_not_used_csv(ROW_PERMUT_MAT_FILE)
    np.savetxt(KEY_FILE, key, fmt=CSV_PRECISION)
    if (use_bias.get()):
        np.savetxt(BIAS_FILE, bias, fmt=CSV_PRECISION)
    else:
        write_not_used_csv(BIAS_FILE)
    with open(ACT_FUNCS_FILE, "w") as file:
        file.write(''.join(activation_functions_seq))
        file.write("\nNumber of iterations: {}".format(iteration_num_selector.get()))
    np.savetxt(ANON_MATRIX_FILE, anomatrix, fmt=CSV_PRECISION)
    tkMessageBox.showinfo(
        "Success", "All done, check CSV files in the program directory")


def show_help():
    help_text = """
        1. Select input csv file or use random int generator (default is generator)
        2. Select options (shuffle columns etc.)
        3. Select number of iterations
        4. Click "Anonise!"

        The output would consist of 7 CSV files:
        0_original_matrix.csv              - Original matrix
        1_class_permutation_matrix.csv     - Matrix of class permutations
        2_column_permutation_matrix.csv    - Matrix of column permutations
        3_row_permutation_matrix.csv       - Matrix of row permutations
        4_key_matrix.csv                   - Key matrix
        5_bias_matrix.csv                  - Bias matrix
        6_activation_functions_applied.txt - List of activation functions applied in the sequence they've been applied
        7_anonised_matrix.csv              - Final anonimysed matrix

        Note: too much anonisation is bad for your health.
    """
    tkMessageBox.showinfo("Help", help_text)


def select_csv():
    global csv_filename
    csv_filename = askopenfilename(
        title="Select file", filetypes=[("csv files", "*.csv")])

root = tk.Tk()

radio_button_value = tk.StringVar()
shuffle_classes = tk.BooleanVar()
shuffle_columns = tk.BooleanVar()
shuffle_rows = tk.BooleanVar()
use_bias = tk.BooleanVar()

top_frame = tk.Frame(root)
top_frame.pack()

tk.Label(
    top_frame, width=40, text='Take matrix from CSV or generate with given size?').pack()

tk.Radiobutton(
    top_frame, text='Import from CSV', variable=radio_button_value, value="csv",
    command=select_csv, width=15, justify="left").pack()
tk.Radiobutton(
    top_frame, text='Generate', variable=radio_button_value, value="generate",
    width=15, justify="left").pack()

size_select_frame = tk.Frame(root)
size_select_frame.pack()
label_rows = tk.Label(size_select_frame, text="Rows:", padx=10)
label_rows.pack(side="left")
row_num_selector = tk.Spinbox(size_select_frame, from_=5, to=100, width=3)
row_num_selector.pack(side="left")

label_cols = tk.Label(size_select_frame, text="Cols:")
label_cols.pack(side="left")
col_num_selector = tk.Spinbox(size_select_frame, from_=5, to=100, width=3)
col_num_selector.pack(side="left")

mid_frame_upper = tk.Frame(root)
mid_frame_upper.pack()

tk.Checkbutton(mid_frame_upper, text="Shuffle classes", variable=shuffle_classes,
               width=15, justify="left").pack()
tk.Checkbutton(mid_frame_upper, text="Shuffle columns", variable=shuffle_columns,
               width=15, justify="left").pack()
tk.Checkbutton(mid_frame_upper, text="Shuffle rows", variable=shuffle_rows,
               width=15, justify="left").pack()
tk.Checkbutton(mid_frame_upper, text="Apply bias", variable=use_bias, width=15,
               justify="left").pack()

label_det = tk.Label(mid_frame_upper, text="Determinant percent")
label_det.pack(side="left")

det_default = tk.StringVar(root)
det_default.set("10")
determinant_selector = tk.Spinbox(
    mid_frame_upper, from_=1, to=15, width=2, textvariable=det_default)
determinant_selector.pack(side="left")

mid_frame_lower = tk.Frame(root)
mid_frame_lower.pack()

label_iters = tk.Label(mid_frame_lower, text="Number of iterations")
label_iters.pack(side="left")

iteration_num_selector = tk.Spinbox(mid_frame_lower, from_=1, to=10, width=2)
iteration_num_selector.pack(side="left")

bottom_frame = tk.Frame(root)
bottom_frame.pack(side="bottom")

FireButton = tk.Button(
    bottom_frame, text="Ananise!", command=take_over_the_world, width=15)
FireButton.pack()

HelpButton = tk.Button(
    bottom_frame, text="Show help", command=show_help, width=15)
HelpButton.pack()

root.mainloop()
