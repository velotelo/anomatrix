## anomatrix.py

Usage help:

`$ python anomatrix.py --help`

###### Usage example:

`$ python anomatrix.py --in-file winequality-red.csv`

Outputs:

anonymised_data.csv         anonimysed data (just multiplication)
anonymised_data_sigm.csv    anonimysed data + sigmoid
anonymised_data_tanh.csv    anonimysed data + tanh
key.csv                     key used in multiplication


## anomatrix_no_norm.py

Usage help:

`$ python anomatrix_no_norm.py --help`

Generates anonimysed matrix by multiplication only without any normalisation.

Each run generates three types of key-matrices:

1. Matrix of integers. Output file is prefixed with: "_int_key"
2. Matrix of floats in half-open range [0;1). Output file is prefixed with:  "_ufloat_key"
3. Matrix of floats in half-open range [-1;1). Output file is prefixed with: "_float_key"

and corresponding to them anonymised matrices.

The determinant of the key matrix should not be in following range:

1. For integer matrix:
    - lower limit is x% of input matrix minimal value or 0 (whatever is smaller)
    - upper limit is x% of input matrix maximal value
    e.g. input matrix minimum is -100 and maximum is 100, percentage is set to 10%. Determinant should not be in range [-10;10]
2. For float matrix of size less than 15x15:
    - lower limit is -1 multiplied by
    - upper limit is 1 multiplied by x%

###### Usage examples:

`$ python anomatrix_no_norm.py --in-file winequality-red.csv --rows 20  --auto-mode`

Will make 15 runs with percentage from 1 to 15%. On each run will generate three keys and corresponding anonymised matrices using 20 rows of winequality-red.csv file as input.

`$ python anomatrix_no_norm.py --in-file winequality-red.csv --determinant 5`

Will make 1 run with determinant limits of 5% and generate three keys and corresponding anonymised matrices using 20 rows of winequality-red.csv file as input.



## anoniser_v2_GUI.py

Pre-requisites:

- numpy
(use `$ pip install numpy`)

Usage:

`$ python anoniser_v2_GUI.py`

For more info click on "Show help" button.
