from __future__ import division
import sys
from numpy import matlib
import numpy as np
import argparse


INT_RAND_MAX = 500   # max value used in integer matrix generator
INT_RAND_MIN = -500  # min value used in integer matrix generator

PRINT_PRECISION = 3  # number of decimal signs to show
CSV_PRECISION = "%.{}f".format(PRINT_PRECISION)


def main(args):
    if not args.in_file:
        if not args.float_input:
            print("Using integer input matrix...")
        if not args.float_key:
            print("Using integer key...")
    generate_results(args)


def generate_integer_matrix(cols, rows):
    return np.random.randint(INT_RAND_MIN, INT_RAND_MAX, size=(cols, rows))


def generate_float_matrix(cols, rows):
    return matlib.rand(cols, rows)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def print_matrices(in_matrix,
                   in_norm_matrix,
                   key,
                   key_norm,
                   out_matrix,
                   sigm_matrix,
                   tanh_matrix,
                   args):

    np.set_printoptions(precision=PRINT_PRECISION)
    np.set_printoptions(suppress=True)

    print("Input matrix:\n %s \n" % in_matrix)
    if not args.float_input:
        print("Input matrix normalized:\n %s \n" % in_norm_matrix)
    print("Key:\n %s \n" % key)
    if not args.float_key:
        print("Key normalized:\n %s \n" % key_norm)
    print("Multiplication result:\n %s \n" % out_matrix)
    print("...with sigmoid:\n %s \n" % sigm_matrix)
    print("...with tanh:\n %s \n" % tanh_matrix)


def parse_csv(args):
    return np.loadtxt(args.in_file,
                      delimiter=args.delimiter,
                      skiprows=1,
                      usecols=range(args.cols),
                      max_rows=args.rows)


def write_csv(args, anomatrix, anomatrix_tahn, anomatrix_sigm,  key):
    np.savetxt("anonymised_data.csv",
               anomatrix,
               fmt=CSV_PRECISION,
               delimiter=args.delimiter)
    np.savetxt("anonymised_data_tanh.csv",
               anomatrix_tahn,
               fmt=CSV_PRECISION,
               delimiter=args.delimiter)
    np.savetxt("anonymised_data_sigm.csv",
               anomatrix_sigm,
               fmt=CSV_PRECISION,
               delimiter=args.delimiter)
    np.savetxt("key.csv", key, fmt=CSV_PRECISION)


def generate_results(args):
    if args.in_file is not None:
        matrix = parse_csv(args)
    else:
        matrix = (
            generate_float_matrix(args.cols, args.rows)
            if args.float_input else
            generate_integer_matrix(args.cols, args.rows)
        )

    normalized_matrix = ((matrix - matrix.min(0)) / matrix.ptp(0)
                         if not args.float_input else matrix)

    while True:
        key = (
            generate_float_matrix(args.cols, args.cols)
            if args.float_key else
            generate_integer_matrix(args.cols, args.cols)
        )
        normalized_key = (
            (key - key.min(0)) / key.ptp(0)
            if not args.float_input else key
        )
        if np.linalg.det(normalized_key) != 0:
            break


    # from IPython.core.debugger import set_trace
    # set_trace()

    anomatrix = np.matmul(normalized_matrix, normalized_key)

    anomatrix_tahn = np.tanh(anomatrix)
    anomatrix_sigm = sigmoid(anomatrix)

    print_matrices(matrix,
                   normalized_matrix,
                   key,
                   normalized_key,
                   anomatrix,
                   anomatrix_sigm,
                   anomatrix_tahn,
                   args)

    write_csv(args, anomatrix, anomatrix_tahn, anomatrix_sigm, normalized_key)


def get_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--in-file", help="input CSV file with dataset")
    parser.add_argument(
        "--delimiter",
        help="CSV file delimiter. Default ';'",
        default=';')
    parser.add_argument(
        "--max-value",
        help="Max value for randomly generated matrix. Default 30",
        type=int,
        default=30)
    parser.add_argument(
        "--float-input",
        help="Whether generated input matrix should consist of float values",
        action="store_true")
    parser.add_argument(
        "--float-key",
        help="Whether generated key should consist of float values",
        action="store_true")
    parser.add_argument(
        "--rows",
        help="Number of rows to read from CSV / use for random generation. Default 10",
        default=10)
    parser.add_argument(
        "--cols",
        help="Number of columns to read from CSV / use for random generation. Default 11",
        default=11)
    return parser.parse_args(argv)


if __name__ == '__main__':
    args = get_args(sys.argv[1:])
    main(args)
