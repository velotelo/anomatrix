from __future__ import division
import sys
from numpy import matlib
import numpy as np
import argparse


INT_RAND_MAX = 500   # max value used in integer matrix generator
INT_RAND_MIN = -500  # min value used in integer matrix generator

PRINT_PRECISION = 3  # number of decimal signs to show
CSV_PRECISION = "%.{}f".format(PRINT_PRECISION)

INT_KEY = 0
UFLOAT_KEY = 1
FLOAT_KEY = 2
KEY_TYPES = {
    INT_KEY: "int_key",
    UFLOAT_KEY: "ufloat_key",
    FLOAT_KEY: "float_key"
}


def main(args):
    generate_and_print_results(args)


def generate_integer_matrix(rows, cols):
    return np.random.randint(INT_RAND_MIN, INT_RAND_MAX, size=(rows, cols))


def generate_float_matrix(rows, cols, low=0):
    return np.random.uniform(low=low, size=(rows, cols))


def print_matrices(in_matrix,
                   key,
                   key_norm,
                   out_matrix,
                   args):

    np.set_printoptions(precision=PRINT_PRECISION)
    np.set_printoptions(suppress=True)

    print("Input matrix:\n{}\n".format(in_matrix))
    print("Key:\n{}\n".format(key))
    if not args.float_key:
        print("Key normalized:\n{}\n".format(key_norm))

    print("DESC:")

    print("Multiplication result:\n{}\n".format(out_matrix))


def parse_csv(args):
    return np.loadtxt(args.in_file,
                      delimiter=args.delimiter,
                      skiprows=1,
                      usecols=range(args.cols),
                      max_rows=args.rows)


def write_csvs(args, matrix, key, key_type, determinant_percent=None):
    filename_suffix = ("_{}_{}.csv".format(determinant_percent, key_type)
                       if args.auto_mode else "_{}.csv".format(key_type))
    matrix_filename = "anomatrix" + filename_suffix
    key_filename = "key" + filename_suffix
    np.savetxt(matrix_filename,
               matrix,
               fmt=CSV_PRECISION,
               delimiter=args.delimiter)
    np.savetxt(key_filename, key, fmt=CSV_PRECISION)


def generate_and_print_results(args):

    def generate_key_and_anomatrix(det_percent):
        for key_type in KEY_TYPES.keys():
            key = generate_key(
                args, det_percent, key_type, matrix.max(), matrix.min())
            anomatrix = np.matmul(matrix, key)
            write_csvs(
                args, anomatrix, key, KEY_TYPES.get(key_type), det_percent)

    if args.in_file is not None:
        matrix = parse_csv(args)
    else:
        print("Generating {} matrix".format(
            "float" if args.float_input else "int"))
        matrix = (
            generate_float_matrix(args.rows, args.cols)
            if args.float_input else
            generate_integer_matrix(args.rows, args.cols)
        )

    if args.auto_mode:
        print("Generating keys and anonymised matrices with determinant range [1,15]%")
        for i in range(1, 16):
            generate_key_and_anomatrix(i)
    else:
        print("Generating key and anonymised matrix with determinant percent {}".format(args.determinant))
        generate_key_and_anomatrix(args.determinant)
    print("All done.")


def generate_key(args, determinant_percent, key_type, matrix_max, matrix_min):
    print("Generating {}...".format(KEY_TYPES.get(key_type)))
    if key_type not in (KEY_TYPES.keys()):
        raise Exception("Unsupported key type {}".format(key_type))
        return

    min_limit = matrix_min * determinant_percent / 100
    max_limit = matrix_max * determinant_percent / 100
    if min_limit > 0:
        min_limit = 0

    # These limits don't make sense on float 0;1 matrices of up to 15x15 size
    # hence the following code: lower limit is offset from zero in both directions
    # whilst the upper limit remains unchanged, cause in this situation
    # it's unlikely that determinant will be >=1 even
    if (key_type != INT_KEY and args.cols < 15):
        max_limit = determinant_percent / 100
        min_limit = -max_limit


    print("Determinant should not be in range [{},{}]".format(min_limit, max_limit))

    attempt = 1
    while True:
        if key_type == INT_KEY:
            key = generate_integer_matrix(args.cols, args.cols)
        elif key_type == UFLOAT_KEY:
            key = generate_float_matrix(args.cols, args.cols)
        else:
            key = generate_float_matrix(args.cols, args.cols, -1)

        det = np.linalg.det(key)
        if ((min_limit > det or det > max_limit) and det != 0):
            print("Key with determinant condition generated on attempt {}".format(attempt))
            # print("Determinant={}".format(det))
            return key

        attempt += 1
        if attempt % 100000 == 0:
            print("Still working... Attempt {}".format(attempt))
            # print("Determinant not in required range. Attempt %s..." % attempt)


def get_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--in-file", help="input CSV file with dataset")
    parser.add_argument(
        "--delimiter",
        help="CSV file delimiter. Default '%(default)s'",
        default=';')
    parser.add_argument(
        "--float-input",
        help="When set, the generated input matrix will consist of float values",
        action="store_true")
    parser.add_argument(
        "--determinant",
        help="Determinant percentage range. Default %(default)s%%",
        type=int,
        default=10)
    parser.add_argument(
        "--auto-mode",
        help="When set, script would run automatically with determinant limit from 1 to 15 and output 15 x 3 matrices and keys",
        action="store_true")
    parser.add_argument(
        "--rows",
        help="Number of rows to read from CSV / use for random generation. Default %(default)s",
        type=int,
        default=10)
    parser.add_argument(
        "--cols",
        help="Number of columns to read from CSV / use for random generation. Default %(default)s",
        type=int,
        default=11)
    return parser.parse_args(argv)


if __name__ == '__main__':
    args = get_args(sys.argv[1:])
    main(args)
