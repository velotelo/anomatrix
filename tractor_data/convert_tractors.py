import csv
import glob
import xlrd


OTHER_BRAND = u"\xd6VRIGA"
CLASSES = [
    "- 55", "56-68", "69-82", "83-96", "97-109", "110-123", "124-136",
    "137-150", "151-164", "165-192", "193-247", "248-300", "301-350",
    "351-400", "401-450", "451-"
]
MONTHS_MAP = {
    "januari": "01",
    "februari": "02",
    "mars": "03",
    "april": "04",
    "maj": "05",
    "juni": "06",
    "juli": "07",
    "augusti": "08",
    "september": "09",
    "oktober": "10",
    "november": "11",
    "december": "12"
}
MISSING_DATA_MON = "2016-06"  # corner case - data is missing

col_range = range(1, 30, 2)
row_range_other = range(2, 35, 2)  # current year
row_range_2016 = range(5, 36, 2)
row_range_2016.insert(0, 2)  # for year 2016 from 2017's files


def process_cell_value(v):
    if isinstance(v, float):
        return int(v)
    elif (isinstance(v, unicode) and v.find(OTHER_BRAND) > 0):
        return u"Other"
    return v


def format_month_year(v):
    v = v.split(" - ")[1]
    m, y = v.split(" ")
    return y + "-" + MONTHS_MAP[m]


data = dict()

for filename in glob.glob('*.xlsx'):
    workbook = xlrd.open_workbook(filename)
    sheet = workbook.sheet_by_index(0)

    month_year = format_month_year(sheet.cell(0, 0).value)

    if (month_year.startswith("2017")):
        # Getting 2016 data from 2017 files
        month_data = [[process_cell_value(sheet.cell_value(r, c)) for c in col_range] for r in row_range_2016]
        valtra_index = month_data[0].index(u'VALTRA')
        for r in range(len(month_data)):
            month_data[r].append(month_data[r].pop(valtra_index))

        data[month_year.replace("2017", "2016")] = month_data

    if (month_year in ["2018-01", "2018-02", "2018-03", "2018-04", "2018-06"]):
        # Getting data for Jan-Apr 2017 from 2018 files
        month_data = [[process_cell_value(sheet.cell_value(r, c)) for c in col_range] for r in row_range_2016]
        valtra_index = month_data[0].index(u'VALTRA')
        for r in range(len(month_data)):
            month_data[r].append(month_data[r].pop(valtra_index))

        data[month_year.replace("2018", "2017")] = month_data

    month_data = [[process_cell_value(sheet.cell_value(r, c)) for c in col_range] for r in row_range_other]

    valtra_index = month_data[0].index(u'VALTRA')
    for r in range(len(month_data)):
        month_data[r].append(month_data[r].pop(valtra_index))

    data[month_year] = month_data


# sanity check that we have same manufacturers
manuf_list = data["2017-05"][0]
for key in data.keys():
    if (data[key][0] != manuf_list):
        raise ValueError("Something wrong with %s" % key)


first_row = manuf_list
first_row.insert(0, "")
for t_class in CLASSES:
    current_index = CLASSES.index(t_class)
    data_per_class = list()
    data_per_class.insert(0, first_row)
    for year_mon in sorted(data.keys()):
        row = data[year_mon][current_index + 1]  # index+1 since first row has manufacturers
        row.insert(0, year_mon)
        data_per_class.append(row)

    out_file_name = t_class + ".csv"
    with open(out_file_name, "wb") as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerows(data_per_class)
