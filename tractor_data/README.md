Script takes all ".xlsx" files in its directory and converts them to .csv files with following transformations:

- csv file per tractor class
- "Accumulated" column is removed
- first row contains manufacturers
- each subsequent row represents a year/month pair
- VALTRA is moved to the rightmost side of the table
- Data for year 2016 is fetched from year-2017 files, except jun 2016
- Data for Jan-Apr, Jun 2017 is fetched from 2018

### Pre-requisites:
1. Python and pip installed
2. xlrd python package installed

    `$ pip install xlrd`

### Usage:

1. Place ".xlsx" files downloaded from https://lantbruksnytt.se/traktorstatistik/ to the same directory with the script
2. Execute script:

	`$ python convert_tractors.py`

3. Script would process each input excel file and would produce a ";"-delimited CSV file named "<class>.csv"
